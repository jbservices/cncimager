package com.jb1services.cncimager.renderer

import java.util.*

class Sector(val sectorType: SectorType, sizeX: Float, sizeY: Float) {
    val sizeX //%
            : Float
    val sizeY //%
            : Float

    init {
        this.sizeX = if (sizeX < 0f) 0f else if (sizeX > 1f) 1f else sizeX
        this.sizeY = if (sizeY < 0f) 0f else if (sizeY > 1f) 1f else sizeY
    }

    companion object {
        fun makeRandomSector(minX: Float, minY: Float, sectorType: SectorType): Sector {
            var minX = minX
            var minY = minY
            minX = if (minX < 0f) 0f else if (minX > 1f) 1f else minX
            minY = if (minY < 0f) 0f else if (minY > 1f) 1f else minY
            val r = Random()
            var sx = 0f
            var sy = 0f
            while (sx == 0f || sx < minX) {
                sx = r.nextFloat()
            }
            if (!sectorType.isNecessarilySymmetric) {
                while (sy == 0f || sy < minY) {
                    sy = r.nextFloat()
                }
            } else sy = sx
            return Sector(sectorType, sx, sy)
        }

        fun makeRandomSector(minX: Float, minY: Float, onEdge: Boolean): Sector {
            var minX = minX
            var minY = minY
            minX = if (minX < 0f) 0f else if (minX > 1f) 1f else minX
            minY = if (minY < 0f) 0f else if (minY > 1f) 1f else minY
            val r = Random()
            val ecs = SectorType::class.java.enumConstants
            var choosen: SectorType? = null
            //select correct random SectorType
            while (choosen == null || choosen.isOnlyOnEdge && !onEdge) {
                choosen = ecs[r.nextInt(ecs.size)]
            }
            var sx = 0f
            var sy = 0f
            while (sx == 0f || sx < minX) {
                sx = r.nextFloat()
            }
            if (!choosen.isNecessarilySymmetric) {
                while (sy == 0f || sy < minY) {
                    sy = r.nextFloat()
                }
            } else sy = sx
            return Sector(choosen, sx, sy)
        }
    }
}