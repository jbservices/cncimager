package com.jb1services.cncimager.main

import com.jb1services.cncimager.renderer.CNCRenderer
import java.awt.BorderLayout
import javax.swing.ImageIcon
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.WindowConstants


fun main() {
    val image = CNCRenderer.provideRandomImage()
    var frame = JFrame()
    frame.setSize(image.width, image.height)
    frame.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
    val label = JLabel()
    label.icon = ImageIcon(image)
    frame.contentPane.add(label, BorderLayout.CENTER)
    frame.setLocationRelativeTo(null)
    frame.pack()
    frame.isVisible = true
}