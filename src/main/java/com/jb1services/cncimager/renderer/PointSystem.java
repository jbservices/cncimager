package com.jb1services.cncimager.renderer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by jonas on 06.09.2017.
 */
public class PointSystem
{
    public static int poinSystemMaxSize = SquareCoordinateRender.maxDisplayCapability / SquareCoordinateRender.fieldSizeInPixels;
    private Point[] points;

    public PointSystem(Point... points)
    {
        this.points = points;
    }

    /**
     * Creates random PointSystem that satisfies @param numOfPoints and @param minDist
     * @param numOfPoints
     * @param minDist
     */
    public PointSystem(int numOfPoints, int minDist)
    {
        List<Point> points = new ArrayList();
        Random r = new Random();
        int maxDistance = poinSystemMaxSize;

        sout("Starting creation of point system!\n");
        while (points.size() < numOfPoints)
        {
            sout("Created " + points.size() + " points so far. Requiring " + numOfPoints+"\n");
            sout("Proceeding with point generation");

            int[] minmax = getMinMaxValuesFromPoints(points);
            if (minmax == null)
            {
                points.add(new Point(r.nextInt(maxDistance+1) - r.nextInt(maxDistance+1), r.nextInt(maxDistance+1) - r.nextInt(maxDistance+1)));
                sout("Creating first point at " + points.get(0).x + "|" + points.get(0).y);
            }
            else
            {
                int distminmax_x = minmax[2] - minmax[0], distminmax_y = minmax[3] - minmax[1];
                int mx = minmax[0]+distminmax_x / 2, my = minmax[1]+distminmax_y / 2;

                int tryx = mx + r.nextInt(maxDistance-distminmax_x) - r.nextInt(maxDistance-distminmax_x);
                int tryy = my + r.nextInt(maxDistance-distminmax_y) - r.nextInt(maxDistance-distminmax_y);

                Point newPoint = new Point(tryx, tryy);
                while (!isPointWithinValidDistanceToExistingPoints(newPoint, minDist, points) || tryx == Integer.MAX_VALUE)
                {
                    if(tryx != Integer.MAX_VALUE){sout("Point is invalid :/ Creating new point.");}
                    tryx = mx + r.nextInt(maxDistance-distminmax_x) - r.nextInt(maxDistance-distminmax_x);
                    tryy = my + r.nextInt(maxDistance-distminmax_y) - r.nextInt(maxDistance-distminmax_y);
                    newPoint = new Point(tryx, tryy);
                    sout("Point was created at " + tryx + "|" + tryy + ". Proceeding with check if point is valid");
                    try
                    {
                        Thread.sleep(200);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                sout("Success! Adding new point "+newPoint.toString()+"\n");
                points.add(newPoint);
            }
        }
        sout("We have enough points! Finishing up PointSystem!");
        Point[] pointss = new Point[points.size()];
        this.points = points.toArray(pointss);
    }

    private boolean isPointWithinValidDistanceToExistingPoints(Point newPoint, int minDist, List<Point> points)
    {
        for (Point p: points)
        {
            if (!(Math.abs(newPoint.x - p.x) >= minDist && (Math.abs(newPoint.y - p.y) >= minDist)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * [0] = minx [1] = miny [2] = maxx [3] = maxy
     * @param points
     * @return
     */
    public static int[] getMinMaxValuesFromPoints(List<Point> points)
    {
        if (points == null || points.size() == 0) {return null;}

        int minx = 0, miny = 0, maxx = 0, maxy = 0;

        int i = 0;
        for (Point p: points)
        {
            if (i == 0)
            {
                minx = p.x; maxx = p.x; miny = p.y; maxy = p.y;
            }
            else
            {
                if (p.x < minx)
                {
                    minx = p.x;
                }
                else if (p.x > maxx)
                {
                    maxx = p.x;
                }

                if (p.y < miny)
                {
                    miny = p.y;
                }
                else if (p.y > maxy)
                {
                    maxy = p.y;
                }
            }
            i++;
        }
        int[] ret = new int[]{minx, miny, maxx, maxy};
        return ret;
    }

    private static List<Point> polygonToListOfPoints(Polygon p)
    {
        List<Point> ps = new ArrayList<>();
        for (int i = 0; i < p.npoints; i++)
        {
            ps.add(new Point(p.xpoints[i], p.ypoints[i]));
        }
        return ps;
    }

    public boolean hasNoIntersectingSides()
    {
        if (this.points.length == 3) {return true;}
        Point firstPoint = this.points[0];

        Line2D[] lines = new Line2D[this.points.length];
        for (int i = 0; i < this.points.length-1; i++)
        {
            lines[i] = new Line2D.Float(points[i].x, points[i].y, points[i+1].x, points[i+1].y);
        }
        lines[this.points.length-1] = new Line2D.Float(points[0].x, points[0].y, points[this.points.length-1].x, points[this.points.length-1].y);

        for (int i = 0; i < lines.length; i++)
        {
            for (int j = 0; j < lines.length; j++)
            {
                if (!(Math.abs(j-i)<=1 || (Math.abs(j-i) == lines.length-1)) && lines[i].intersectsLine(lines[j]))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public static PointSystem getRandomSimpleSystem(int numOfPoints, int minDist)
    {
        PointSystem ps = new PointSystem(numOfPoints, minDist);
        sout("Finished creation of Point system! It "+(!ps.hasNoIntersectingSides() ? "has" : "has no") + " intersecting sides!"); //ps.hasNoIntersecting... does not terminate when == true
        while (!ps.hasNoIntersectingSides())
        {   sout("Seems like we need a new system...\n");
            ps = new PointSystem(numOfPoints, minDist);
            sout("Finished creation of Point system! It "+(!ps.hasNoIntersectingSides() ? "has" : "has no") + " intersecting sides!");
        }
        return ps;
    }

    public static PointSystem getRandomSimpleSystem()
    {
        return getRandomSimpleSystem(5,1);
    }

    @Override
    public String toString()
    {
        String ret = "";
        int i = 0;
        for (Point p: points)
        {
            ret+="P"+i+": "+p.toString().replace(p.getClass().getName(),"")+"\n";
            i++;
        }
        return ret;
    }

    public Polygon toPolygon()
    {
        int[] xpoints = new int[this.points.length];
        int[] ypoints = new int[this.points.length];

        int i = 0;
        for (Point p: this.points)
        {
            xpoints[i] = p.x;
            ypoints[i] = p.y;
            i++;
        }
        return new Polygon(xpoints,ypoints,xpoints.length);
    }

    public GeneralPath toGeneralPath()
    {
        double[] xpoints = new double[this.points.length+1];
        double[] ypoints = new double[this.points.length+1];

        int i = 0;
        for (Point p: this.points)
        {
            xpoints[i] = p.x;
            ypoints[i] = p.y;
            i++;
        }
        xpoints[this.points.length] = this.points[0].x;
        ypoints[this.points.length] = this.points[0].y;

        GeneralPath polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, xpoints.length);

        polyline.moveTo (xpoints[0], ypoints[0]);

        for (int index = 1; index < xpoints.length; index++)
        {
                 polyline.lineTo(xpoints[index], ypoints[index]);
        }

        return polyline;
    }

    public static BufferedImage polygonComponentToBufferedImage(PolygonComponent pc)
    {
        Polygon p = pc.getPolygon();

        //V1
        BufferedImage image = new BufferedImage(p.getBounds().width, p.getBounds().height, BufferedImage.TYPE_INT_ARGB);
        //V2
        //BufferedImage image = new BufferedImage(pc.polyLine.getBounds().width, pc.polyLine.getBounds().height, BufferedImage.TYPE_INT_RGB);

        //V1
        //Graphics g = image.getGraphics();
        //V2
        Graphics2D g2 = (Graphics2D) image.getGraphics();

        g2.setStroke(new BasicStroke(3));
        RenderingHints rh = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHints(rh);
        //g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

        g2.setColor(Color.BLACK);

        //V1
        //g.drawPolygon(p);
        //V2
        //g2.draw(pc.polyLine);
        g2.draw(p);
        return image;
    }

    public void display()
    {
        Displayer display = new Displayer(this);
        display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display.setVisible(true);
    }

    public Point[] getPoints()
    {
        return this.points;
    }


    class Displayer extends JFrame implements KeyListener
    {
        private PolygonComponent pc;

        public Displayer(PointSystem ps)
        {
            setTitle("Displayer");
            setSize(PointSystem.poinSystemMaxSize,PointSystem.poinSystemMaxSize);
            pc = (new PolygonComponent(ps));
            this.add(pc);
        }

        @Override
        public void keyTyped(KeyEvent e) {
            this.pc.setPointSystem(PointSystem.getRandomSimpleSystem());
            System.out.println("TEST");
            this.pc.regenerateFromPointSystem();
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyPressed(KeyEvent e) {
            System.out.println("test");
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyReleased(KeyEvent e) {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public static class PolygonComponent extends JComponent
    {
        protected PointSystem pointSystem;
        private Polygon polygon;
        private GeneralPath polyLine;

        public PolygonComponent(PointSystem ps)
        {
            this.pointSystem = ps;
            regenerateFromPointSystem();
            //scalePolygon(2,2, true);
        }

        public void regenerateFromPointSystem()
        {
            this.polygon = pointSystem.toPolygon();
            this.polyLine = pointSystem.toGeneralPath();
            int[] minmax = getMinMaxValuesFromPoints(polygonToListOfPoints(polygon));
            //sout("minx = "+minmax[0]+". miny = "+minmax[1]);
            shiftPolygon(minmax[0]*-1,0, true); shiftGeneralPath(minmax[0]*-1, 0, true);
            shiftPolygon(0,minmax[1]*-1, true); shiftGeneralPath(0, minmax[1]*-1, true);
            scalePolygon(SquareCoordinateRender.fieldSizeInPixels, SquareCoordinateRender.fieldSizeInPixels, true);
            scaleGeneralPath(SquareCoordinateRender.fieldSizeInPixels, SquareCoordinateRender.fieldSizeInPixels, true);
        }

        public Polygon getPolygon()
        {
            return polygon;
        }

        public PointSystem getPointSystem() {
            return pointSystem;
        }

        public void setPointSystem(PointSystem pointSystem) {
            this.pointSystem = pointSystem;
        }

        public GeneralPath getPolyLine() {
            return polyLine;
        }

        public void setPolyLine(GeneralPath polyLine) {
            this.polyLine = polyLine;
        }

        @Override
        public void paintComponent(Graphics g)
        {
            Graphics2D g2 = (Graphics2D) g;
            g2.setColor(Color.BLACK);
            //g.drawRect(0,0,50,50);
            //BufferedImage image = new BufferedImage(polygon.getBounds().width, polygon.getBounds().height, BufferedImage.TYPE_INT_RGB);
            //Graphics2D g3 = (Graphics2D) image.getGraphics();
            //g3.setColor(Color.BLACK);
            //g3.drawPolygon(polygon);

            //V1
            /*
            g2.setStroke(new BasicStroke(3));
            RenderingHints rh = new RenderingHints(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHints(rh);
            g2.drawPolygon(polygon);
            */
            BufferedImage bImg = polygonComponentToBufferedImage(this);
            g2.drawImage(bImg, 0, 0, null);
            //V2
            //g2.draw(polyLine);
            //g2.drawImage(image, 0, 0, this);
        }

        private Polygon shiftPolygon(int deltax, int deltay, boolean overwrite)
        {
            Polygon p = new Polygon(polygon.xpoints, polygon.ypoints, polygon.npoints);
            p.translate(deltax,deltay);
            if (overwrite) this.polygon = p;
            return p;
        }

        private GeneralPath shiftGeneralPath(int deltax, int deltay, boolean overwrite)
        {
            AffineTransform at = AffineTransform.getTranslateInstance(deltax, deltay);
            GeneralPath gp = (GeneralPath) this.polyLine.createTransformedShape(at);
            if (overwrite) {this.polyLine = gp;}
            return gp;
        }

        private Polygon scalePolygon(int xfac, int yfac, boolean overwrite)
        {
            Polygon p = new Polygon(polygon.xpoints, polygon.ypoints, polygon.npoints);
            int sx = p.xpoints[0], sy = p.ypoints[0];

            for (int i = 0; i < polygon.npoints; i++)
            {
                p.xpoints[i] = p.xpoints[i] * xfac;
                p.ypoints[i] = p.ypoints[i] * yfac;
            }
            //p = shiftPolygon(sx - p.xpoints[0], sy - p.ypoints[0], false);
            if (overwrite) this.polygon = p;
            return p;
        }

        private GeneralPath scaleGeneralPath(int xfac, int yfac, boolean overwrite)
        {
            AffineTransform at = AffineTransform.getScaleInstance(xfac, yfac);
            GeneralPath gp = (GeneralPath) this.polyLine.createTransformedShape(at);
            if (overwrite) {this.polyLine = gp;}
            return gp;
        }
    }

    private static void sout(String s)
    {
        System.out.println(s);
    }
}
