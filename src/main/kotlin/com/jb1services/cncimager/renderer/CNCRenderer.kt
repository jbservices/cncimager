package com.jb1services.cncimager.renderer

import mu.KotlinLogging
import java.awt.BasicStroke
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

private val logger = KotlinLogging.logger {}

/**
 * Basically this creates a random CNC training design
 * It knows 6 sectors (basically the design is a rectangle devided into 6 equally large squares - the sectors)
 * The sectors are parts of the design that have s
 * @author jonas
 */
object CNCRenderer {
    const val SECTORS_PER_LINE = 3
    const val LINES_PER_COLUMN = 2
    const val SCALE_NUMBERS_TO = 80
    var bgColor = Color.CYAN
    var fgColor = Color.BLUE
    var contourColor = Color.BLACK
    val DEBUG_COLOR = Color.YELLOW
    private const val contourThickness = 5
    private const val SCALE = 5
    const val sectorSideLength = 40 * SCALE //randomized
    const val marginWidth = 10 * SCALE //randomized
    private const val valuesBarWidth = 10 * SCALE
    private val sectors = arrayOfNulls<Sector>(LINES_PER_COLUMN * SECTORS_PER_LINE) //always 6
    private var designImg = BufferedImage(sectorSideLength * SECTORS_PER_LINE + marginWidth * 2, sectorSideLength * LINES_PER_COLUMN + marginWidth * 2, BufferedImage.TYPE_INT_ARGB)
    private var design_g2d = designImg.createGraphics()
    private var designWithValuesImg = BufferedImage(sectorSideLength * SECTORS_PER_LINE + marginWidth * 2 + valuesBarWidth * 2, sectorSideLength * LINES_PER_COLUMN + marginWidth * 2 + valuesBarWidth * 2, BufferedImage.TYPE_INT_ARGB)
    private var designWith_Values_g2d = designWithValuesImg.createGraphics()
    private var hasReloaded = false
    private var hasDrawn = false
    private fun fillWithSectorsOfOneType(edges: SectorType?, nonEdges: SectorType?) {
        if (edges!!.isOnlyOnEdge && !nonEdges!!.isOnlyOnEdge) {
            for (y in 0 until LINES_PER_COLUMN) {
                for (x in 0 until SECTORS_PER_LINE) {
                    //generate correct sector if on edge
                    sectors[y * SECTORS_PER_LINE + x] = if (isEdge(x, y, SECTORS_PER_LINE, LINES_PER_COLUMN)) Sector.makeRandomSector(0.2f, 0.2f, edges) else Sector.makeRandomSector(0.2f, 0.2f, nonEdges)
                }
            }
        } else {
            throw IllegalArgumentException("Sorry, but please only use edge types for 'edges' and non-edge types for 'non-eedges'")
        }
    }

    private fun isEdge(x: Int, y: Int, sectorsPerLine: Int, linesPerColumns: Int): Boolean {
        val ret = (x == 0 || x == sectorsPerLine - 1) && (y == 0 || y == linesPerColumns - 1)
        logger.debug {if (ret) "is edge" else ""}
        return ret
    }

    private fun fillWithRandomSectors() {
        for (y in 0 until LINES_PER_COLUMN) {
            for (x in 0 until SECTORS_PER_LINE) {
                //generate correct sector if on edge
                sectors[y * SECTORS_PER_LINE + x] = if (isEdge(x, y, SECTORS_PER_LINE, LINES_PER_COLUMN)) Sector.makeRandomSector(0.2f, 0.2f, true) else
                    Sector.makeRandomSector(0.2f, 0.2f, false)
                logger.debug { "Generating sector " + sectors[y * SECTORS_PER_LINE + x]!!.sectorType }
            }
        }
    }

    @JvmOverloads
    fun drawSectors(edges: SectorType? = null, nonEdges: SectorType? = null): BufferedImage {
        logger.debug {"--\\nDrawing to image"}

        if (!hasReloaded) {
            if (edges == null && nonEdges == null) reload() else reload(edges, nonEdges)
        }

        //Filling base rect
        design_g2d.color = bgColor
        val sbx = sectorSideLength * SECTORS_PER_LINE + marginWidth * 2
        val sby = sectorSideLength * LINES_PER_COLUMN + marginWidth * 2
        logger.debug {"Filling base rect ${designImg.width}x${designImg.height}. Should be ${sbx}x${sby}"}
        design_g2d.fillRect(0, 0, designImg.width, sby /*bImg.getHeight()*/)

        //filling non-margin rect
        design_g2d.color = fgColor
        design_g2d.fillRect(marginWidth, marginWidth, sbx - marginWidth * 2, sby - marginWidth * 2)
        val xstart = marginWidth
        val ystart = marginWidth
        for (sy in 0 until LINES_PER_COLUMN) {
            val y = ystart + sy * sectorSideLength
            for (sx in 0 until SECTORS_PER_LINE) {
                val x = xstart + sx * sectorSideLength
                val sector = sectors[sy * 3 + sx]
                val st = sector!!.sectorType
                logger.debug {"${sy * 3}$sx: Now drawing $st onlyOnEdge=${st.isOnlyOnEdge} at x=$x y=$y"}
                st.render(design_g2d, sector, sx, sy, x, y)
            }
        }
        hasDrawn = true
        logger.debug {"--"}
        return designImg
    }

    private fun completeImage(): BufferedImage {
        val xs = sectorSideLength * SECTORS_PER_LINE + marginWidth * 2 + valuesBarWidth * 2
        val ys = sectorSideLength * LINES_PER_COLUMN + marginWidth * 2 + valuesBarWidth * 2
        designWith_Values_g2d.color = Color.black
        designWith_Values_g2d.stroke = BasicStroke(4F)
        designWith_Values_g2d.drawLine(0, 0, xs, 0)
        designWith_Values_g2d.drawLine(0, ys, xs, ys)
        designWith_Values_g2d.drawLine(0, 0, 0, ys)
        designWith_Values_g2d.drawLine(xs, 0, xs, ys)
        val updexes = IntArray(SECTORS_PER_LINE * 2 + 4)
        val downdexes = IntArray(SECTORS_PER_LINE * 2) //basically the pixel values at which we need a marker that tells the pixel value normalized as in without scaling
        val edgeIndeces = intArrayOf(0, SECTORS_PER_LINE - 1, (LINES_PER_COLUMN - 1) * SECTORS_PER_LINE, (LINES_PER_COLUMN - 1) * SECTORS_PER_LINE + SECTORS_PER_LINE - 1) //should be 0,2,3,5 with 3*2
        val nonEdgeIndeces = IntArray(SECTORS_PER_LINE * 2 - 4)
        var count = 0
        for (i in 1 until SECTORS_PER_LINE * LINES_PER_COLUMN - 1) {
            if (i % 3 != 0 && (i + 1) % 3 != 0) {
                nonEdgeIndeces[count] = i
                count++
            }
        }
        var updone = 0
        var downdone = 0
        for (index in edgeIndeces) {
            val st = sectors[index]!!.sectorType
            val up = index < 3
            logger.debug {"Found sector type $st for edge index $index"}
            if (st == SectorType.QUARTER_CIRCLE_INCISION || st == SectorType.EDGE_LINEAR_CUT || st == SectorType.ROUND_EDGE) {
                if (up) {
                    if (index == 0) //first sector first line
                    {
                        updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + sectors[index]!!.sizeX * sectorSideLength).toInt()
                    } else  //first sector last line
                    {
                        updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + (sectorSideLength - sectors[index]!!.sizeX * sectorSideLength)).toInt()
                    }
                    updone++
                } else {
                    if (index == SECTORS_PER_LINE * (LINES_PER_COLUMN - 1)) //last sector in last line
                    {
                        downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + sectors[index]!!.sizeX * sectorSideLength).toInt()
                    } else  //first sector in last line
                    {
                        downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + (sectorSideLength - sectors[index]!!.sizeX * sectorSideLength)).toInt()
                    }
                    downdone++
                }
            } else if (st == SectorType.HALF_CIRCLE_INCISION) {
                if (up) {
                    updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + sectorSideLength / 2)
                    updone++
                    updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + sectorSideLength / 2 + sectorSideLength * sectors[index]!!.sizeX / 2).toInt()
                    updone++
                } else {
                    downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + sectorSideLength / 2)
                    downdone++
                    downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + sectorSideLength / 2 + sectorSideLength * sectors[index]!!.sizeX / 2).toInt()
                    downdone++
                }
            }
        }
        for (index in nonEdgeIndeces) {
            val st = sectors[index]!!.sectorType
            val up = index < 3
            logger.debug {"Found sector type $st for nonedge index $index"}
            if (st == SectorType.HALF_CIRCLE_INCISION) {
                if (up) {
                    updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + sectorSideLength / 2)
                    updone++
                    updexes[updone] = (valuesBarWidth + marginWidth + index * sectorSideLength + sectorSideLength / 2 + sectorSideLength * sectors[index]!!.sizeX / 2).toInt()
                    updone++
                } else {
                    downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + sectorSideLength / 2)
                    downdone++
                    downdexes[downdone] = (valuesBarWidth + marginWidth + (index - SECTORS_PER_LINE) * sectorSideLength + sectorSideLength / 2 + sectorSideLength * sectors[index]!!.sizeX / 2).toInt()
                    downdone++
                }
            }
        }
        drawIndexes(updexes, downdexes)
        //leftdexes
        return designWithValuesImg
    }

    private fun drawIndexes(updexes: IntArray, downdexes: IntArray) {
        val xts = totalSizeX
        val yts = totalSizeY
        val xds = xts - drawingSizeX
        val yds = yts - drawingSizeY
        val scaleFactor = (SCALE_NUMBERS_TO + 0.0) / xts
        logger.debug {"xts = $xts yts = $yts xds = $xds yds = $yds"}
        logger.debug {"Scale factor = $scaleFactor. Scale numbers to = $SCALE_NUMBERS_TO"}
        updexes[updexes.size - 4] = valuesBarWidth
        updexes[updexes.size - 3] = xts - valuesBarWidth
        updexes[updexes.size - 2] = valuesBarWidth + marginWidth
        updexes[updexes.size - 1] = xts - valuesBarWidth - marginWidth
        logger.debug {"Found " + updexes.size + " updexes!"}
        for (updex in updexes) {
            logger.debug {updex}
            if (updex != 0) {
                designWith_Values_g2d.drawLine(updex, 0, updex, valuesBarWidth / 4)
                val num = ((updex - valuesBarWidth) * scaleFactor)
                designWith_Values_g2d.drawString(String.format("%.2f", num), updex, valuesBarWidth / 2)
            }
        }
        logger.debug {"Found " + downdexes.size + " downdexes!"}
        for (downdex in downdexes) {
            logger.debug {downdex}
            if (downdex != 0) {
                designWith_Values_g2d.drawLine(downdex, yts, downdex, yts - valuesBarWidth / 4)
                val num = ((downdex - valuesBarWidth) * scaleFactor)
                designWith_Values_g2d.drawString(String.format("%.2f", num), downdex, yts - valuesBarWidth / 2)
                //designWith_Values_g2d.drawString(((downdex - valuesBarWidth) * scaleFactor).toString(), downdex, yts - valuesBarWidth / 2)
            }
        }
        designWith_Values_g2d.drawLine(0, valuesBarWidth, valuesBarWidth / 4, valuesBarWidth)
        designWith_Values_g2d.drawString(((yts - valuesBarWidth * 2) * scaleFactor).toString(), valuesBarWidth / 2, valuesBarWidth)
        designWith_Values_g2d.drawLine(0, valuesBarWidth + marginWidth, valuesBarWidth / 4, valuesBarWidth + marginWidth)
        designWith_Values_g2d.drawString(((yts - valuesBarWidth * 2 - marginWidth) * scaleFactor).toString(), valuesBarWidth / 2, valuesBarWidth + marginWidth)
        designWith_Values_g2d.drawLine(0, yts - valuesBarWidth, valuesBarWidth / 4, yts - valuesBarWidth)
        designWith_Values_g2d.drawString((0 * scaleFactor).toString(), valuesBarWidth / 2, yts - valuesBarWidth)
        designWith_Values_g2d.drawLine(0, yts - valuesBarWidth - marginWidth, valuesBarWidth / 4, yts - valuesBarWidth - marginWidth)
        designWith_Values_g2d.drawString((marginWidth * scaleFactor).toString(), valuesBarWidth / 2, yts - valuesBarWidth - marginWidth)
        designWith_Values_g2d.drawImage(designImg, valuesBarWidth, valuesBarWidth, null)
    }

    fun drawImage(): BufferedImage {
        drawSectors()
        return completeImage()
    }

    private val totalSizeX: Int
        private get() = sectorSideLength * SECTORS_PER_LINE + marginWidth * 2 + valuesBarWidth * 2
    private val totalSizeY: Int
        private get() = sectorSideLength * LINES_PER_COLUMN + marginWidth * 2 + valuesBarWidth * 2
    private val drawingSizeX: Int
        private get() = totalSizeX - valuesBarWidth * 2
    private val drawingSizeY: Int
        private get() = totalSizeY - valuesBarWidth * 2

    @JvmOverloads
    fun display(edges: SectorType? = null, nonEdges: SectorType? = null, withValuesBar: Boolean = true) {
        if (!hasDrawn) {
            if (edges == null && nonEdges == null) drawSectors() else drawSectors(edges, nonEdges)
        }
        if (withValuesBar) completeImage()
        val bdis = BuffDisplay(if (withValuesBar) designWithValuesImg else designImg, true)
    }

    private fun reload(edges: SectorType? = null, nonEdges: SectorType? = null) {
        designImg = BufferedImage(sectorSideLength * 3 + marginWidth * 2, sectorSideLength * 3 + marginWidth * 2, BufferedImage.TYPE_INT_ARGB)
        design_g2d = designImg.createGraphics()
        if (edges == null && nonEdges == null) fillWithRandomSectors() else fillWithSectorsOfOneType(edges, nonEdges)
        hasReloaded = true
        logger.debug {"--"}
    }

    fun clear() {
        designImg = BufferedImage(sectorSideLength * SECTORS_PER_LINE + marginWidth * 2, sectorSideLength * LINES_PER_COLUMN + marginWidth * 2, BufferedImage.TYPE_INT_ARGB)
        design_g2d = designImg.createGraphics()
        designWithValuesImg = BufferedImage(sectorSideLength * SECTORS_PER_LINE + marginWidth * 2 + valuesBarWidth * 2, sectorSideLength * LINES_PER_COLUMN + marginWidth * 2 + valuesBarWidth * 2, BufferedImage.TYPE_INT_ARGB)
        designWith_Values_g2d = designWithValuesImg.createGraphics()
    }

    fun BufferedImage.toByteArray(format: String): ByteArray {
        val baos = ByteArrayOutputStream()
        ImageIO.write(this, format, baos)
        return baos.toByteArray()
    }

    fun provideRandomImage() : BufferedImage {
        clear()
        fillWithRandomSectors()
        return drawImage()
    }
}