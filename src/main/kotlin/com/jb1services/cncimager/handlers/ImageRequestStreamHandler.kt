package com.jb1services.cncimager.handlers

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import com.jb1services.cncimager.renderer.CNCRenderer
import com.jb1services.cncimager.renderer.CNCRenderer.toByteArray
import java.io.InputStream
import java.io.OutputStream

class ImageRequestStreamHandler: RequestStreamHandler {
    override fun handleRequest(input: InputStream?, output: OutputStream, context: Context?) {
        val media = CNCRenderer.provideRandomImage().toByteArray("PNG")
        output.write(media)
    }
}