/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cncimager.renderer

import java.awt.Graphics
import java.awt.image.BufferedImage
import javax.swing.JComponent
import javax.swing.JFrame

/**
 *
 * @author Jonas
 */
class BuffDisplay @JvmOverloads constructor(private val img: BufferedImage, display: Boolean = false) : JFrame() {
    init {
        val rImg = img
        this.add(
            object : JComponent() {
                public override fun paintComponent(g: Graphics) {
                    g.drawImage(rImg, 0, 0, null)
                }
            }
        )
        this.setSize(img.width, img.height)
        defaultCloseOperation = EXIT_ON_CLOSE
        //this.setUndecorated(true);
        display(display)
    }

    private fun display(display: Boolean) {
        this.isVisible = display
    }
}