import org.gradle.internal.impldep.org.eclipse.jgit.lib.ObjectChecker.type
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	kotlin("jvm") version "1.7.20"
}

group = "com.jb1services"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}


dependencies {
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib:1.7.20")
	// https://mvnrepository.com/artifact/com.amazonaws/aws-lambda-java-core
	implementation("com.amazonaws:aws-lambda-java-core:1.2.1")
	implementation("io.github.microutils:kotlin-logging-jvm:3.0.2")
	// https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
	implementation ("ch.qos.logback:logback-classic:1.4.5")

}

tasks.withType<Jar> {

	val include = setOf("kotlin-stdlib-1.7.20.jar")

	configurations.runtimeClasspath.get()
			.filter { it.name in include }
			.map { zipTree(it) }
			.also { from(it) }
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
