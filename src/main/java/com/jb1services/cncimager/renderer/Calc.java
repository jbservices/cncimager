/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cncimager.renderer;

import java.util.Random;

/**
 *
 * @author Jonas
 */
public class Calc
{
    public static float getRandomNonHardFloat()
    {
        Random r = new Random();
        int tens = r.nextInt(6);
        int tenths = r.nextInt(10);
        float f = Float.valueOf(tens+"."+tenths);
        if (f <= 0.0) return getRandomNonHardFloat();
        return f;
    }
}
