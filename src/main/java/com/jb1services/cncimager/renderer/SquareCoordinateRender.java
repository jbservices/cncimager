/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jb1services.cncimager.renderer;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 *
 * @author Jonas
 */
public class SquareCoordinateRender
{
    public static int maxDisplayCapability = 1000, maxdcx = maxDisplayCapability, maxdcy = maxDisplayCapability;
    public static int fieldSizeInPixels = 50;
    public static int zeroPointFraction = 4; //how big the zero point is compared to a field
    public static int pointFraction = 4; //how big each point is compared to a field

    private PointSystem.PolygonComponent referencePointSystem;

    private int finalWidth, finalHeight;
    private BufferedImage finalImage;

    public SquareCoordinateRender(PointSystem ps, boolean display)
    {
        //making sure that there are no empty pixels
        this.referencePointSystem = new PointSystem.PolygonComponent(ps);
        finalWidth = maxdcx - maxdcx % fieldSizeInPixels; finalHeight = maxdcy - maxdcy % fieldSizeInPixels;
        this.finalImage = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_INT_ARGB);

        drawSquares();
        drawAchsisAndZeroPoint();
        drawPointSystem();
        if (display)
        {
            BuffDisplay bd = new BuffDisplay(finalImage, true);
        }
    }

    public SquareCoordinateRender(PointSystem ps)
    {
        this(ps, false);
    }

    public BufferedImage getFinalImage() {
        return finalImage;
    }



    private void drawSquares()
    {
        Graphics2D g2d = (Graphics2D) finalImage.createGraphics();

        g2d.setColor(Color.black);
        //g2d.drawRect(50, 50, 100,100);

        for (int y = 0; y < maxdcy; y+=fieldSizeInPixels)
        {
            for (int x = 0; x <= maxdcx; x+=fieldSizeInPixels)
            {
                g2d.drawRect(x, y, fieldSizeInPixels, fieldSizeInPixels);
            }
        }
    }

    private void drawAchsisAndZeroPoint()
    {
        int zerox = finalWidth / 2, zeroy = finalHeight / 2;

        Graphics2D g2d = (Graphics2D) finalImage.createGraphics();
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(2));
        g2d.drawLine(0, zeroy, finalWidth-1, zeroy);
        g2d.drawLine(zerox, 0, zerox, finalHeight-1);

        g2d.fillRect(zerox-fieldSizeInPixels/(zeroPointFraction)/2, zeroy-fieldSizeInPixels/(zeroPointFraction)/2, fieldSizeInPixels/(zeroPointFraction), fieldSizeInPixels/(zeroPointFraction));
    }

    private void drawPointSystem()
    {
        Graphics2D g2d = (Graphics2D) finalImage.createGraphics();
        g2d.setColor(Color.black);

        g2d.setStroke(new BasicStroke(3));

        RenderingHints rh = new RenderingHints(
        RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHints(rh);

        g2d.draw(referencePointSystem.getPolygon());

        int[] xpoints = this.referencePointSystem.getPolygon().xpoints;
        int[] ypoints = this.referencePointSystem.getPolygon().ypoints;

        System.out.println(xpoints.length + " points");
        int fSize = fieldSizeInPixels / pointFraction;
        for (int i = 0; i < xpoints.length; i++)
        {
            int px = xpoints[i], py = ypoints[i]; int dx = px-fSize/2, dy = py-fSize/2;
            System.out.println("now drawing point " + px+"|"+py + " at " + dx+"|"+dy);
            //g2d.fillRect(xpoints[i]-bl/4, ypoints[i]-bl/4, bl/2, bl/2);
            g2d.fillRect(dx, dy, fSize, fSize);
        }
    }
}
