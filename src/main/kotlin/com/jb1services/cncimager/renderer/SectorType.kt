package com.jb1services.cncimager.renderer

import java.awt.Graphics2D

enum class SectorType(val isNecessarilySymmetric: Boolean, val isOnlyOnEdge: Boolean, val render: (g2d: Graphics2D, sector: Sector, sx: Int, sy: Int, x: Int, y: Int) -> Unit) {
    QUARTER_CIRCLE_INCISION(true, true, {
        g2d: Graphics2D, sector: Sector, sx: Int, sy: Int, x: Int, y: Int ->
        run {
            val awidth = (sector.sizeX * CNCRenderer.sectorSideLength).toInt()
            val aheight = (sector.sizeY * CNCRenderer.sectorSideLength).toInt()
            if (sy == 0) {
                if (sx == 0) {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillArc(x - awidth, y - awidth, awidth * 2, aheight * 2, 0, -90)
                } else {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillArc(x + CNCRenderer.sectorSideLength - awidth, y - awidth, awidth * 2, aheight * 2, -90, -90)
                }
            } else {
                if (sx == 0) {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillArc(x - awidth, y + CNCRenderer.sectorSideLength - aheight, awidth * 2, aheight * 2, 0, 90)
                } else {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillArc(x + CNCRenderer.sectorSideLength - awidth, y + CNCRenderer.sectorSideLength - aheight, awidth * 2, aheight * 2, 90, 90)
                }
            }
        }
    }),
    HALF_CIRCLE_INCISION(true, false, {
        g2d: Graphics2D, sector: Sector, sx: Int, sy: Int, x: Int, y: Int ->
        run {
            val awidth = (sector.sizeX * CNCRenderer.sectorSideLength).toInt()
            val aheight = awidth / 2
            if (sy == 0) {
                g2d.color =
                    CNCRenderer.bgColor
                g2d.fillArc(x + CNCRenderer.sectorSideLength / 2 - awidth / 2, y - aheight, awidth, aheight * 2, 0, -180)
            } else {
                g2d.color =
                    CNCRenderer.bgColor
                g2d.fillArc(x + CNCRenderer.sectorSideLength / 2 - awidth / 2, y + CNCRenderer.sectorSideLength - aheight, awidth, aheight * 2, 0, 180)
            }
        }
    }),
    ROUND_EDGE(true, true, {
        g2d: Graphics2D, sector: Sector, sx: Int, sy: Int, x: Int, y: Int -> 
        run {
            val awidth = (sector.sizeX * CNCRenderer.sectorSideLength).toInt()
            val aheight = (sector.sizeY * CNCRenderer.sectorSideLength).toInt()
            if (sy == 0) {
                if (sx == 0) {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillRect(x, y, awidth, aheight)
                    g2d.color = CNCRenderer.fgColor
                    g2d.fillArc(x, y, awidth * 2, aheight * 2, 180, -90)
                } else {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillRect(x + CNCRenderer.sectorSideLength - awidth, y, awidth, aheight)
                    g2d.color = CNCRenderer.fgColor
                    g2d.fillArc(x + CNCRenderer.sectorSideLength - awidth * 2, y, awidth * 2, aheight * 2, 0, 90)
                }
            } else {
                if (sx == 0) {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillRect(x, y + CNCRenderer.sectorSideLength - aheight, awidth, aheight)
                    g2d.color = CNCRenderer.fgColor
                    g2d.fillArc(x, y + CNCRenderer.sectorSideLength - aheight * 2, awidth * 2, aheight * 2, 270, -90)
                } else {
                    g2d.color = CNCRenderer.bgColor
                    g2d.fillRect(x + CNCRenderer.sectorSideLength - awidth, y + CNCRenderer.sectorSideLength - aheight, awidth, aheight)
                    g2d.color = CNCRenderer.fgColor
                    g2d.fillArc(x + CNCRenderer.sectorSideLength - awidth * 2, y + CNCRenderer.sectorSideLength - aheight * 2, awidth * 2, aheight * 2, 270, 90)
                }
            }
        }
    }),
    EDGE_LINEAR_CUT(true, true, {
            g2d: Graphics2D, sector: Sector, sx: Int, sy: Int, x: Int, y: Int ->
        run {
            val xpoints = kotlin.IntArray(3)
            val ypoints = kotlin.IntArray(3)
            if (sy == 0) {
                if (sx == 0) {
                    xpoints[0] = x
                    xpoints[1] =
                        (x + sector.sizeX * CNCRenderer.sectorSideLength).toInt()
                    xpoints[2] = x
                    ypoints[0] = y
                    ypoints[1] = y
                    ypoints[2] =
                        (y + sector.sizeY * CNCRenderer.sectorSideLength).toInt()
                } else {
                    xpoints[0] = x + CNCRenderer.sectorSideLength
                    xpoints[1] =
                        (xpoints[0] - CNCRenderer.sectorSideLength * sector.sizeX).toInt()
                    xpoints[2] = xpoints[0]
                    ypoints[0] = y
                    ypoints[1] = y
                    ypoints[2] =
                        (y + sector.sizeY * CNCRenderer.sectorSideLength).toInt()
                }
            } else {
                if (sx == 0) {
                    xpoints[0] = x
                    xpoints[1] =
                        (x + sector.sizeX * CNCRenderer.sectorSideLength).toInt()
                    xpoints[2] = x
                    ypoints[0] = y + CNCRenderer.sectorSideLength
                    ypoints[1] = ypoints[0]
                    ypoints[2] =
                        (ypoints[1] - sector.sizeY * CNCRenderer.sectorSideLength).toInt()
                } else {
                    xpoints[0] = x + CNCRenderer.sectorSideLength
                    xpoints[1] =
                        (xpoints[0] - CNCRenderer.sectorSideLength * sector.sizeX).toInt()
                    xpoints[2] = xpoints[0]
                    ypoints[0] = y + CNCRenderer.sectorSideLength
                    ypoints[1] = ypoints[0]
                    ypoints[2] =
                        (ypoints[1] - sector.sizeY * CNCRenderer.sectorSideLength).toInt()
                }
            }
            val p = java.awt.Polygon(xpoints, ypoints, 3)
            g2d.color =
                CNCRenderer.bgColor
            g2d.fillPolygon(p)
        }
    })
}